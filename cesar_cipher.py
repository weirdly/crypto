def e_cesar_cipher(message, key):
    encrypted_message = ''
    for char in message:
        if 65 <= ord(char) <= 90:
            encrypted_message += chr(ord(char) + key % 65)
        else:
            encrypted_message += chr(ord(char) + key % 97)
    return encrypted_message


def d_cesar_cipher(message, key):
    decrypted_message = ''
    for char in message:
        if 65 <= ord(char) <= 90:
            decrypted_message += chr(ord(char) - key % 65)
        else:
            decrypted_message += chr(ord(char) - key % 97)
    return decrypted_message


def h_cesar_cipher(message):
    for x in range(1, 27):
        print('key: ', x, ' message: ', d_cesar_cipher(message=message, key=x))


def e_improved_cesar_cipher(message, keys):
    if len(message) != len(keys):
        print('This key not valid')
        return
    encrypted_message = ''
    for char, key in zip(message, keys):
        encrypted_message += e_cesar_cipher(char, key)
    return encrypted_message


def d_improved_cesar_cipher(message, keys):
    if len(message) != len(keys):
        print('This key not valid')
        return
    decrypted_message = ''
    for char, key in zip(message, keys):
        decrypted_message += d_cesar_cipher(char, key)
    return decrypted_message


def test_cesar_cipher():
    original_message = 'Hello, World!'
    key = 25

    encrypted_message = e_cesar_cipher(original_message, key)
    decrypted_message = d_cesar_cipher(encrypted_message, key)

    print(f'Original message: {original_message}', end='\n\n')
    print(f'Encrypted message: {encrypted_message}', end='\n\n')
    print(f'Decrypted message: {decrypted_message}', end='\n\n')

    assert original_message == decrypted_message
    print('It works :)', end='\n\n')
    print('------------------------------------------------------')
    print("Let's hack it ^_^")
    print('------------------------------------------------------', end='\n\n')
    h_cesar_cipher(encrypted_message)
    print('Hope you find it')
