def prime_in_range(end_n=1000):
    if not isinstance(end_n, int):
        raise TypeError("Arguments must be an integer")

    if end_n < 2:
        raise ValueError('Must be greater than 2')

    sieve = list(range(2, (end_n + 1)))

    for v in sieve:
        for index, value in enumerate(sieve):
            if value != v and value % v == 0:
                sieve.pop(index)
    return sieve


def is_prime(p):
    if not isinstance(p, int):
        raise TypeError('Integer error')

    if p <= 1:
        raise ValueError('Must be greater than 1')

    for a in range(2, (p + 1)):
        if p % a == 0 and p != a:
            return False
    return True


if __name__ == '__main__':
    pass
